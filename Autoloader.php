<?php
/**
 * Simple autoloader, so we don't need Composer just for this.
 */
class Autoloader
{
    public static function register()
    {
        spl_autoload_register(function ($class) {
            $base_dir = __DIR__;
            $file = $base_dir . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php';
            if (!file_exists($file)) {
                throw new \Exception("File is not found: {$file}");
            }
            require $file;
        });
    }
}
Autoloader::register();