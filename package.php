<?php
class Package
{
    private $name;
    private $weight;

/**
 * Посылки
 * @param  $name Имя посылки
 * @param  $weight Вес посылки
 */
    public function __construct($name,$weight) {
        $this->name = $name;
        $this->weight = $weight;
    }

    public function getWeight() {
        return $this->weight;
    }

	/**
	 * @return mixed
	 */
	public function getName() {
		return $this->name;
	}
}
?>