<?php
class Truck extends Transport implements CanDeliver, CalculateCost {
/**
 * Доставка посылки
 * @param  $package Экземпляр посылки
 */
    public function deliverPackage(Package $package) {
        if ($package->getWeight() > 10000) {
            echo "Посылка слишком тяжела для грузовика.<br/><br/>";
            return;
        }
        echo "Доставку выполнил $this->name<br/>";
    }
/**
 * Расчёт стоимости 
 * @param  $package Экземпляр посылки
 */
    public function calculateCost(Package $package) {
        $price = $package->getWeight()*1.5;
        echo "Стоимость доставки для транспорта <$this->name> составит $price<br/>";
    }
}
?>