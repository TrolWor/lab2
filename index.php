<?php
require_once('Autoloader.php');
$package1 = new Package("Коробка",100);
$package2 = new Package("Турбина",2500);
$truck = new Truck("Грузовик");
$train = new Train("Поезд");
$plane = new Plane("Самолёт");
$truck->deliverPackage($package1);
$train->deliverPackage($package1);
$plane->deliverPackage($package1);
$truck->deliverPackage($package2);
$train->deliverPackage($package2);
$plane->deliverPackage($package2);
$train->calculateCost($package1);
$truck->calculateCost($package1);
$plane->calculateCost($package1);
?>